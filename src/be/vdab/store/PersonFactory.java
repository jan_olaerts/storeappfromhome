package be.vdab.store;

import java.util.Random;

public class PersonFactory {

    private static Random random = new Random();
    private static String[] characteristics = new String[] {"Lazy", "Energetic", "Macho"};

    public static Person getPerson(String name, int age, Gender gender) {
        String characteristic = characteristics[random.nextInt(3)];

        Person person;
        if(characteristic.equals("Lazy")) {
            person = new LazyPerson(name, age, gender);
        } else if (characteristic.equals("Energetic")) {
            person = new EnergeticPerson(name, age, gender);
        } else {
            person = new MachoPerson(name, age, gender);
        }

        return person;
    }
}