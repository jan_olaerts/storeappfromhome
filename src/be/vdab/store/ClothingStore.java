package be.vdab.store;

public class ClothingStore extends ItemStore {

    private String name = "Fashion Store: D&B";
    private String description = "A place where you can buy clothes";

    public ClothingStore() {
        super();
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public String getName() {
        return this.name;
    }

//    @Override
//    public void enterStore(IPerson person) {
//        System.out.println(person.getName() + " walks into " + this.getName());
//    }
}