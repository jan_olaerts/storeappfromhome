package be.vdab.store;

public interface IStore {

    public void enterStore(IPerson person);
    public String getName();
}