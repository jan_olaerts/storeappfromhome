package be.vdab.store;

public class EnergeticPerson extends Person {

    public EnergeticPerson(String name, int age, Gender gender) {
        super(name, age, gender);
    }

    @Override
    public String move() {
        return " jumps ";
    }
}