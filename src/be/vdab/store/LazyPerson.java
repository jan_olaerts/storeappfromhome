package be.vdab.store;

public class LazyPerson extends Person {

    public LazyPerson(String name, int age, Gender gender) {
        super(name, age, gender);
    }

    @Override
    public String move() {
        return " saunters ";
    }
}