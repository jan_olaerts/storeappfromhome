package be.vdab.store;

import java.util.Formatter;
import java.util.Scanner;

public abstract class ItemStore implements IStore {
    private static Scanner scanner = new Scanner(System.in);

    private static String[] items;
    private static double[] prices;

    public ItemStore() {
        if(this instanceof ClothingStore) {
            this.items = new String[] {"Red t-shirt", "Blue jeans", "Pair of socks", "Leather jacket"};
            this.prices = new double[] {51.55, 65.99, 12.99, 315.89};
        } else if(this instanceof ComputerStore) {
            this.items = new String[] {"Gaming computer", "Laptop 3000", "4K-TrueColor-Monitor"};
            this.prices = new double[] {2589.99, 1579.99, 567.99};
        } else if (this instanceof Bar) {
            this.items = new String[] {"Water", "Beer", "Coffee", "Tea", "Soda", "A little bag of salty chips"};
            this.prices = new double[] {2.5, 4.5, 2.3, 3.2, 3.5, 2.5};
        }
    }

    public void showMenu() {
        Formatter formatter = new Formatter();

        System.out.println("What do you want to buy from " + this.getName() + "?");
        System.out.println("Items for sale: " + items.length);
        for(int i = 0; i < items.length; i++) {
            System.out.println(i + ". " + items[i] + " | price: " + prices[i] + " Euros");
//            formatter.format(i + ". " + items[i] + " price: " + prices[i]);
        }
//        System.out.println(formatter.toString());
        System.out.println("-1. To exit " + this.getName());
    }

    public static int getChoice(Person person, ItemStore store) {
        int choice = scanner.nextInt();

        try {
            switch(choice) {
                case 0:

                    if(checkSaldo(person, 0)) {
                        break;
                    }

                    buy(person, 0);
                    addToCart(person, 0);
                    break;
                case 1:

                    if(checkSaldo(person, 1)) {
                        break;
                    }

                    if(store instanceof Bar && person.getAge() < 19) {
                        System.out.println("You are too young to buy beer!");
                        break;
                    }

                    buy(person, 1);
                    addToCart(person, 1);
                    break;
                case 2:

                    if(checkSaldo(person, 2)) {
                        break;
                    }

                    buy(person, 2);
                    addToCart(person, 2);
                    break;
                case 3:

                    if(checkSaldo(person, 3)) {
                        break;
                    }

                    buy(person, 3);
                    addToCart(person, 3);
                    break;
                case 4:

                    if(checkSaldo(person, 4)) {
                        break;
                    }

                    buy(person, 4);
                    addToCart(person, 4);
                    break;
                case 5:

                    if(checkSaldo(person, 5)) {
                        break;
                    }

                    buy(person, 5);
                    addToCart(person, 5);
                    break;
                default:

                    if(choice != -1) {
                        System.out.println("Choose a valid item: ");
                    }

                    break;
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Choose a valid item: ");
        }

        return choice;
    }

    public static boolean checkSaldo(Person person, int n) {

        if(person.getBudget() - prices[n] < 0) {
            System.out.println("You do not have the budget to buy " + items[n] + " for " + prices[n] + " Euros");
            return true;
        } else {
            return false;
        }
    }

    public static void buy(Person person, int n) {
        Formatter formatter = new Formatter();

        System.out.println(person.getName() + " buys a " + items[n] + " for " + prices[n] + " Euros");
        person.setBudget(person.getBudget() - prices[n]);
        formatter.format("You have %.2f Euros left to spend", person.getBudget());
        System.out.println(formatter.toString());
    }

    public static void addToCart(Person person, int n) {
        person.getCart().enterItem(items[n], prices[n]);
    }

    @Override
    public String getName() {
        return this.getName();
    }

    @Override
    public void enterStore(IPerson person) {
        System.out.println(person.getName() + person.move() + "into " + this.getName());
    }
}