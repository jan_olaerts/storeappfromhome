package be.vdab.store;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Formatter;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Person person = makePerson();
        startSimulation(person);
    }

    public static Person makePerson() {

        System.out.println("Please insert your name: ");
        String name;
        while(true) {
            try {
                name = scanner.nextLine();

                if(name.contains("0") || name.contains("1") || name.contains("2") || name.contains("3") || name.contains("4") || name.contains("5") || name.contains("6") ||
                        name.contains("7") || name.contains("8") || name.contains("9")) {

                    throw new RuntimeException();
                }

                break;
            } catch(RuntimeException e) {
                System.out.println("Enter a valid name: ");
            }
        }

        System.out.println("Now enter your age: ");
        int age;
        while(true) {
            try {
                age = scanner.nextInt();
                if(age < 2) {
                    System.out.println("You are too young to shop!");
                    throw new RuntimeException();
                }
                if(age > 150) {
                    System.out.println("You are too old to be a person");
                    throw new RuntimeException();
                }
                break;
            } catch (RuntimeException e) {
                System.out.println("Enter a valid age: ");
            } finally {
                scanner.nextLine();
            }
        }

        System.out.println("Now enter your gender (1: Man, 2: Woman): ");
        int gender;
        while(true) {
            try {
                gender = scanner.nextInt();
                if(gender != 1 && gender != 2) {
                    throw new RuntimeException();
                }
                break;
            } catch (RuntimeException e) {
                System.out.println("Enter a valid gender (1: Man, 2: Woman)");
            }
        }

//        Person person = new Person(name, age, gender == 1 ? Gender.MAN : Gender.WOMAN);
        Person person = PersonFactory.getPerson(name, age, gender == 1 ? Gender.MAN : Gender.WOMAN);
        return person;
    }

    public static void startSimulation(Person person) {
        Formatter formatter = new Formatter();

        LocalDateTime nowDate = LocalDateTime.now();
        System.out.println("We are " + nowDate.getDayOfWeek() + " " + nowDate.getDayOfMonth() + " " + nowDate.getMonth() + " " + nowDate.getYear());

        Random random = new Random();
        String[] weatherOptions = new String[] {"sunny", "cloudy", "rainy", "stormy"};
        System.out.println("It is a " + weatherOptions[random.nextInt(4)] + " day\n");

        ShoppingCenter shoppingCenter = new ShoppingCenter();
        shoppingCenter.enterStore(person);

        System.out.println("You can have " + person.getCart().getMaxItems() + " items in your cart");
        formatter.format("Your budget is %.0f Euros", person.getBudget());
        System.out.println(formatter.toString());

        int place = person.walkToPlace();
        int choice;
        while (place != -1) {
            switch(place) {
                case 1:
                    ItemStore clothingStore = new ClothingStore();
                    clothingStore.enterStore(person);

                    clothingStore.showMenu();
                    choice = ItemStore.getChoice(person, clothingStore);
                    while(choice != -1) {
                        clothingStore.showMenu();
                        choice = ItemStore.getChoice(person, clothingStore);
                    }

                    place = person.walkToPlace();
                    break;
                case 2:
                    ItemStore computerStore = new ComputerStore();
                    computerStore.enterStore(person);

                    computerStore.showMenu();
                    choice = ItemStore.getChoice(person, computerStore);
                    while(choice != -1) {
                        computerStore.showMenu();
                        choice = ItemStore.getChoice(person, computerStore);
                    }

                    place = person.walkToPlace();
                    break;
                case 3:
                    ItemStore bar = new Bar();
                    bar.enterStore(person);

                    bar.showMenu();
                    choice = ItemStore.getChoice(person, bar);
                    while(choice != -1) {
                        bar.showMenu();
                        choice = ItemStore.getChoice(person, bar);
                    }

                    place = person.walkToPlace();
                    break;
            }
        }

        System.out.println(person.getName() + person.move() + "to exit of " + shoppingCenter.getName());
        // printing content of cart
        System.out.println("Content of " + person.getName() + "'s cart: ");
        for(int i = 0; i < person.getCart().getItemList().length; i++) {

            if(person.getCart().getItemList()[i] == null) {
                return;
            }
            System.out.println(person.getCart().getItemList()[i] + " | price: " + person.getCart().getPriceList()[i] + " Euros");
        }
    }
}