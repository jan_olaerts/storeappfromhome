package be.vdab.store;

public enum Gender {
    MAN, WOMAN;
}