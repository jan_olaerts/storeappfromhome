package be.vdab.store;

public class MachoPerson extends Person {

    public MachoPerson(String name, int age, Gender gender) {
        super(name, age, gender);
    }

    @Override
    public String move() {
        return " walks in a macho way ";
    }
}