package be.vdab.store;

public class Bar extends ItemStore {

    private String name = "Bar: The drunken shopper";
    private String description = "Bar";

    public Bar() {
        super();
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public String getName() {
        return this.name;
    }

//    @Override
//    public void enterStore(IPerson person) {
//        System.out.println(person.getName() + " walks into " + this.getName());
//    }
}