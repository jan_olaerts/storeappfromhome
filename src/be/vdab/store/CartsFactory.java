package be.vdab.store;

import java.util.Random;

public class CartsFactory {

    private static Random random = new Random();
    private static int[] sizes = new int[] {5, 10, 15};

    public static int generateCart() {
        int size = random.nextInt(3);

        if(size == 1) {
            return sizes[0];
        } else if (size == 2) {
            return sizes[1];
        } else {
            return sizes[2];
        }
    }
}