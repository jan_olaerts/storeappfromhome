package be.vdab.store;

public interface IPerson {

    public int getAge();
    public ICart getCart();
    public Gender getGender();
    public String getName();
    public int walkToPlace();
    public String move();
}