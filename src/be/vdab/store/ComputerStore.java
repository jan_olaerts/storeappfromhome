package be.vdab.store;

public class ComputerStore extends ItemStore {

    private String name = "Computer Store: Fresh bytes";
    private String description = "A store where you can buy computers";

    public ComputerStore() {
        super();
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public String getName() {
        return this.name;
    }

//    @Override
//    public void enterStore(IPerson person) {
//        System.out.println(person.getName() + " walks into " + this.getName());
//    }
}