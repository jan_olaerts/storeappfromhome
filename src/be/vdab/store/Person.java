package be.vdab.store;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public abstract class Person implements IPerson {

    private static Scanner scanner = new Scanner(System.in);

    private String name;
    private int age;
    private Gender gender;
    private ICart cart;
    private double budget;

    public Person(String name, int age, Gender gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.cart = new SmallCart(CartsFactory.generateCart());
        this.budget = (double) 100 + new Random().nextInt(5000);
    }

    @Override
    public int getAge() {
        return this.age;
    }

    @Override
    public ICart getCart() {
        return this.cart;
    }

    @Override
    public Gender getGender() {
        return this.gender;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public double getBudget() {
        return this.budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public void printStoreOptions() {
        System.out.println("1. " + new ClothingStore().getName());
        System.out.println("2. " + new ComputerStore().getName());
        System.out.println("3. " + new Bar().getName());
        System.out.println("4. Print Stores");
        System.out.println("-1. Exit the " + new ShoppingCenter().getName());
    }

    @Override
    public int walkToPlace() {
        System.out.println("Which shop do you want to enter, " + this.getName() + "?");
        this.printStoreOptions();

        int choice;
        while(true) {
            try {
                choice = scanner.nextInt();
                if(choice < -1 || choice > 4) {
                    throw new RuntimeException();
                }

                if (choice == 4) {
                    System.out.println("Which shop do you want to enter, " + this.getName() + "?");
                    throw new InputMismatchException();
                }
                break;
            } catch (InputMismatchException e) {
                this.printStoreOptions();
            } catch(RuntimeException e) {
                System.out.println("Choose a valid shop to enter: ");
                this.printStoreOptions();
            } finally {
                scanner.nextLine();
            }
        }

        return choice;
    }

    public abstract String move();

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", cart=" + cart +
                ", budget=" + budget +
                '}';
    }
}