package be.vdab.store;

import java.util.Arrays;

public class SmallCart implements ICart {

    private final int MAX_ITEMS;
    private final String[] itemList;
    private final double[] priceList;
    private static int itemCount = 0;

    public SmallCart(int MAX_ITEMS) {
        this.MAX_ITEMS = MAX_ITEMS;
        this.itemList = new String[MAX_ITEMS];
        this.priceList = new double[MAX_ITEMS];
    }

    @Override
    public void enterItem(String item, double price) {
        if(this.isFull()) {
            System.out.println("Not possible to add " + item + " to cart");
            System.out.println("You have reached the maximum of items in your cart");
            return;
        } else {
            this.itemList[itemCount] = item;
            this.priceList[itemCount] = price;
            itemCount++;
        }
    }

    @Override
    public int getItemCount() {
        return itemCount;
    }

    @Override
    public int getMaxItems() {
        return this.MAX_ITEMS;
    }

    @Override
    public boolean isFull() {
        return getItemCount() == MAX_ITEMS;
    }

    @Override
    public String[] getItemList() {
        return this.itemList;
    }

    @Override
    public double[] getPriceList() {
        return this.priceList;
    }

    @Override
    public String toString() {
        return "SmallCart{" +
                "MAX_ITEMS=" + MAX_ITEMS +
                ", itemList=" + Arrays.toString(itemList) +
                ", itemCount=" + itemCount +
                '}';
    }
}