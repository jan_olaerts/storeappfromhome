package be.vdab.store;

public class ShoppingCenter implements IStore {

    private final IStore[] stores = new IStore[] {new ClothingStore(), new ComputerStore(), new Bar()};
    private final String name = "Super Shopping Center";
    private final String description = "The best shopping center in the world";

    @Override
    public void enterStore(IPerson person) {
        System.out.println(person.getName() + person.move() + "into " + this.getName());
    }

    public IStore[] getStores() {
        return this.stores;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }
}